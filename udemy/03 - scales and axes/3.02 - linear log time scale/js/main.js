/*
*    main.js
*    Mastering Data Visualization with D3.js
*    3.2 - Linear scales
*/

svg = d3.select("#chart-area")
    .append("svg")
        .attr("width", "400")
        .attr("height", "400");

addRules(60)

d3.json("data/buildings.json").then(function(data){
    console.log(data);

    maxHeight = 0
    data.forEach(d => {
        d.height = +d.height;

        if(d.height > maxHeight){
            maxHeight = d.height
        }
    });

    height = svg.attr("height");
    width = svg.attr("width");

    //=====================

    //LINEAR SCALE
    var yScaleLinear = d3.scaleLinear()
        .domain([0, maxHeight]) // values from dataset
        .range([0, height]); // pixels from the canvas
    console.log(yScaleLinear(100)) // 48.3
    console.log(yScaleLinear(414)) // 200
    console.log(yScaleLinear(700)) // 338.2
    console.log(yScaleLinear.invert(48.3)) // 100
    console.log(yScaleLinear.invert(200)) // 414
    console.log(yScaleLinear.invert(338.2)) // 700

    //LOGARITHMIC SCALE
    var yScaleLog = d3.scaleLog()
        .domain([300, 150000]) // values from dataset
        .range([0, height]) // pixels from the canvas
        .base(10);
    console.log(yScaleLog(500)) // 32.9
    console.log(yScaleLog(5000)) // 181.1
    console.log(yScaleLog(50000)) // 329.3
    console.log(yScaleLog.invert(32.9)) // 500
    console.log(yScaleLog.invert(181.1)) // 5000
    console.log(yScaleLog.invert(329.3)) // 50000

    //TIME SCALE
    var yScaleTime = d3.scaleTime()
        .domain([new Date(2000, 0, 1), new Date(2001, 0, 1)]) // values from dataset
        .range([0, height]); // pixels from the canvas
    console.log(yScaleTime(new Date(2000, 7, 1))) // 232
    console.log(yScaleTime(new Date(2000, 2, 1))) // 66.5
    console.log(yScaleTime(new Date(2000, 10, 25))) // 359
    console.log(yScaleTime.invert(232.8)) // Tue Aug 01 2000
    console.log(yScaleTime.invert(66.5)) // Wed Mar 01 2000
    console.log(yScaleTime.invert(360)) // sun nov 25 2000

    //ORDINAL SCALE - color
    var scaleOrdinal_color = d3.scaleOrdinal()
        .domain(["AFRICA","N. AMERICA", "EUROPE", "S. AMERICA", "ASIA", "AUSTRALIA"]) // values from dataset
        //.range(["RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "GREY"]); // color in the provided options
        .range([d3.schemeCategory10]); // color in the chromatic option
        //https://github.com/d3/d3-scale-chromatic for search for more options

    console.log(scaleOrdinal_color("AFRICA"))
    console.log(scaleOrdinal_color("ASIA"))
    console.log(scaleOrdinal_color("ANTARCTICA"))
    console.log(scaleOrdinal_color("PANGEA"))

    //^^^^^^^^^^^^^^^^^^^^

    var rects = svg.selectAll("rect")
        .data(data)
            .enter()
            .append("rect")
            .attr("y", 0)
            .attr("x", function(d, i){
                return (i * 60);
            })
            .attr("width", 40)
            .attr("height", function(d){
                return yScaleLinear(d.height);
            })
            .attr("fill", function(d) {
                return "grey";
            });

});
