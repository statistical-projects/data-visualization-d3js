/*
*    main.js
*    Mastering Data Visualization with D3.js
*    3.9 - Margins and groups
*/

var margin = { left:100, right:10, top:10, bottom:100 };

var 
canvasWidth = 600 - margin.left - margin.right,
canvasHeight = 400 - margin.top - margin.bottom;

var g = d3.select("#chart-area")
    .append("svg")
        .attr("width", canvasWidth + margin.left + margin.right)
        .attr("height", canvasHeight + margin.top + margin.bottom)
    .append("g") // group element
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");

svg = d3.select("#chart-area svg");
addRules(50);

d3.json("data/buildings.json").then(function(data){
    // console.log(data);

    data.forEach(function(d) {
        d.height = +d.height;
    });

    var xScaleBand = d3.scaleBand()
        .domain(data.map(function(d){
            return d.name;
        })) // MAP function, return all the values
        .range([0, canvasWidth]) 
        .paddingInner(0.3) // distance between the values (bars)
        .paddingOuter(0.3); // disgance between the border of the graphic

    var yScaleLinear = d3.scaleLinear()
        .domain([
            d3.min(data, function(d){
                return d.height;
            }), // MIN funciton
            d3.max(data, function(d){
                return d.height;
            }) // MAX funciton
        ]) 
        .range([0, canvasHeight]);

    // OR you could do

    returned_value = d3.extent(data, function(d){
        return d.height;
    }) // EXTENT funciton

    var yScaleLinear = d3.scaleLinear()
        .domain(
            d3.extent(data, function(d){
                return d.height;
            }) // EXTENT funciton
        ) 
        .range([0, canvasHeight]);

    var rects = g.selectAll("rect")
        .data(data)
        
    rects.enter()
        .append("rect")
            .attr("y", 0)
            .attr("x", function(d){ return xScaleBand(d.name); })
            .attr("width", xScaleBand.bandwidth)
            .attr("height", function(d){ return yScaleLinear(d.height); })
            .attr("fill", "grey");

})