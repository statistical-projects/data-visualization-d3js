/*
*    main.js
*    Mastering Data Visualization with D3.js
*    3.7 - D3 min, max, and extent
*/

var svg = d3.select("#chart-area")
    .append("svg")
    .attr("width", "800")
    .attr("height", "500");

addRules(50);

d3.json("data/buildings.json").then(function(data){
    console.log(data);

    canvasHeight = svg.attr("height")
    canvasWidth = svg.attr("width")

    data.forEach(d => {
        d.height = +d.height;
    });

    var x = d3.scaleBand()
        .domain(data.map(function(d){
            return d.name;
        }))
        .range([0, canvasWidth])
        .paddingInner(0.3) // distance between the values (bars)
        .paddingOuter(0.3); // disgance between the border of the graphic

    var y = d3.scaleLinear()
        .domain([0, d3.max(data, function(d){
            return d.height;
        })])
        .range([0, canvasHeight]);

    var rects = svg.selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("y", 0)
        .attr("x", function(d){
            return x(d.name);
        })
        .attr("width", x.bandwidth)
        .attr("height", function(d){
            return y(d.height);
        })
        .attr("fill", "grey");

});