/*
*    main.js
*    Mastering Data Visualization with D3.js
*    3.11 - Making a bar chart
*/

// it will be divided into 3 parts
// 1. Params & definition of canvas
// 2. Definition of third parties elements
// 3. Plot
// 3.1. import & data prep
// 3.2. scalings
// 3.3. axis
// 3.4. input of values elements




// =========================
// 1. Params & definition of canvas
// =========================

var margin = { left:100, right:10, top:10, bottom:150 };

var width = 600 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// var flag = true;

var t = d3.transition().duration(750);

var svg = d3.select("#chart-area")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)

var g = svg.append("g")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");




// =========================
// 2. Definition of global elements
// =========================


    // Add legends to the graph
    // X Label
    g.append("text")
        .attr("class", "x axis-label")
        .attr("x", width / 2)
        .attr("y", height + 140)
        .attr("font-size", "20px")
        .attr("text-anchor", "middle")
        .text("The word's tallest buildings");

    // Y Label
    g.append("text")
        .attr("class", "y axis-label")
        .attr("x", - (height / 2))
        .attr("y", -60)
        .attr("font-size", "20px")
        .attr("text-anchor", "middle")
        .attr("transform", "rotate(-90)")
        .text("Height (m)");


    // =========================
    // 2.2. scalings
    // =========================

    // X Scale
    var x = d3.scaleBand()
    .range([0, width])
    .paddingInner(0.2)
    .paddingOuter(0.3);

    // Y Scale
    var y = d3.scaleLinear()
    .range([height, 0]);


    // =========================
    // 2.3. axis
    // =========================

    var xAxisGroup = g.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height +")");

    var yAxisGroup = g.append("g")
        .attr("class", "y axis");





// =========================
// 3. Plot
// =========================

d3.json("data/buildings.json").then(function(data){

    // =========================
    // 3.1. import & data prep
    // =========================

    console.log(data);

    //cast the height value into integer
    data.forEach(function(d){
        d.height = +d.height;
    });

    // =========================
    // 3.1. interval for updates
    // =========================

    i = 0;
    d3.interval(function(){
        var newData = data;
        update(newData)

        console.log("new update " + i)
        i++
    }, 1000);

    update(data);

    //addRules()
});






function update(data){
    // field used for value in y axis
    value = "height"

    // =========================
    // 3.2. scalings
    // =========================

    //scale the category 
    x.domain(data.map(function(d){ return d.name; })); // provide the column which has the categories

    //scale the numeric values
    y.domain([
        0, 
        d3.max(data, function(d){
            return d.height;
        })
    ]);

    //ORDINAL SCALE - color
    var scaleOrdinal_color = d3.scaleOrdinal()
        .domain(data.map(function(d){return d.name})) // values from dataset
         .range(["ORANGE", "GRAY", "GRAY", "GRAY", "GRAY", "GRAY", "GRAY"]); // color in the provided options
        // .range([d3.schemeCategory10]); // color in the chromatic option
        //https://github.com/d3/d3-scale-chromatic for search for more options
    

    // =========================
    // 3.3. axis
    // =========================
    
    var xAxisCall = d3.axisBottom(x)//receive the object which is the axis        
    //var xAxisCall = d3.axisTop(x);
        // .tickSizeOuter(3) //size of the outer tickers
        // .tickSizeInner(3) //size of the inner tickers
        // .tickSize(10) //size of all ticks
        // .ticks(5) // number of total ticks
        // .tickFormat(d3.format(",.0f")) // the format of the tick (could be a function as well)
        // .tickValues([0, 200, 400, 600, 800]) // explicit values
    ;
    xAxisGroup.attr("transform", "translate(0, " + height + ")") // position into the 0, bottom value
        .call(xAxisCall)
        .selectAll("text")
            .attr("y", "10")
            .attr("x", "-5")
            .attr("text-anchor", "end")
            .attr("transform", "rotate(-40)");

    var yAxisCall = d3.axisLeft(y)
    // var yAxisCall = d3.axisRight(y)
        .ticks(3) //number of elements in which it will breaking the value
        .tickFormat(function(d){
            return d + "m";
        });
    yAxisGroup.call(yAxisCall);


    // =========================
    // 3.4. clean and input of values elements
    // =========================


    // bar chart
    var rects = g.selectAll("rect")
        .data(data)
    
    // EXIT old elements not present in new data.
    rects.exit()
        .attr("fill", "red")
    .transition(t)
        .attr("y", y(0))
        .attr("height", 0)
        .remove();

     // ENTER new elements present in new data...
     rects.enter()
     .append("rect")
        .attr("fill", function(d){ 
            console.log("com " + d.name + " a cor retornada: " + scaleOrdinal_color(d.name))
            return scaleOrdinal_color(d.name)
        })
        .attr("y", y(0))
        .attr("height", 0)
        .attr("x", function(d){ return x(d.name) })
        .attr("width", x.bandwidth())
        // AND UPDATE all (old and new) elements present in new data.
        .merge(rects)
        .transition(t) //comment to take off the transition
            .attr("x", function(d){ return x(d.name) })
            .attr("width", x.bandwidth())
            .attr("y", function(d){ return y(d[value]); })
            .attr("height", function(d){ return height - y(d[value]); });

   // scatter plot
   /*
   var circles = g.selectAll("circle")
        .data(data)
    
    // EXIT old elements not present in new data.
    circles.exit()
        .attr("fill", "red")
    .transition(t)
        .attr("y", y(0))
        .remove();

     // ENTER new elements present in new data...
     circles.enter()
     .append("circle")
        .attr("fill", function(d){ 
            console.log("com " + d.name + " a cor retornada: " + scaleOrdinal_color(d.name))
            return scaleOrdinal_color(d.name)
        })
        .attr("cy", y(0))
        .attr("r", x.bandwidth() / 2)
        // AND UPDATE all (old and new) elements present in new data.
        .merge(circles)
        .transition(t) //comment to take off the transition
            .attr("cx", function(d){ return x(d.name) + x.bandwidth() / 2})
            .attr("cy", function(d){ return y(d[value]); })
            .attr("r", x.bandwidth() / 2)
    */
}