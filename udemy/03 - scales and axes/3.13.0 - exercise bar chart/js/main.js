/*
*    main.js
*    Mastering Data Visualization with D3.js
*    3.11 - Making a bar chart
*/

// it will be divided into 3 parts
// 1. Params & definition of canvas
// 2. Definition of third parties elements
// 3. Plot
// 3.1. import & data prep
// 3.2. scalings
// 3.3. axis
// 3.4. input of values elements



// =========================
// 1. Params & definition of canvas
// =========================

var margin = { left:100, right:10, top:10, bottom:100 };

var width = 600 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

var svg = d3.select("#chart-area")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)

var g = svg.append("g")
        .attr("transform", "translate(" + margin.left 
            + ", " + margin.top + ")");


// =========================
// 2. Definition of third parties elements
// =========================

// Add legends to the graph
// X Label
g.append("text")
    .attr("class", "x axis-label")
    .attr("x", width / 2)
    .attr("y", height + 80)
    .attr("font-size", "20px")
    .attr("text-anchor", "middle")
    .text("Starbucks Revenue Plot");

// Y Label
g.append("text")
    .attr("class", "y axis-label")
    .attr("x", - (height / 2))
    .attr("y", -60)
    .attr("font-size", "20px")
    .attr("text-anchor", "middle")
    .attr("transform", "rotate(-90)")
    .text("Revenue");



// =========================
// 3. Plot
// =========================

d3.json("data/revenues.json").then(function(data){

    // =========================
    // 3.1. import & data prep
    // =========================

    console.log(data);

    //cast the revenue value into integer
    data.forEach(function(d){
        d.revenue = +d.revenue;
    });

    //cast the profit value into integer
    data.forEach(function(d){
        d.profit = +d.profit;
    });

    
    
    // =========================
    // 3.2. scalings
    // =========================

    //scale the category 
    var x = d3.scaleBand()
        .domain(data.map(function(d){ return d.month; })) // provide the column which has the categories
        .range([0, width])
        .paddingInner(0.2)
        .paddingOuter(0.3);

    //scale the numeric values
    var y = d3.scaleLinear()
        .domain([
            0, 
            d3.max(data, function(d){
                return d.revenue;
            })
        ])
        .range([height, 0]);

    //ORDINAL SCALE - color
    var scaleOrdinal_color = d3.scaleOrdinal()
        .domain(data.map(function(d){return d.month})) // values from dataset
         .range(["GRAY", "GRAY", "GRAY", "GRAY", "GRAY", "GRAY", "ORANGE"]); // color in the provided options
        // .range([d3.schemeCategory10]); // color in the chromatic option
        //https://github.com/d3/d3-scale-chromatic for search for more options
    

    // =========================
    // 3.3. axis
    // =========================
    
    var xAxisCall = d3.axisBottom(x); //receive the object which is the axis
    g.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0, " + height + ")") // position into the 0, bottom value
        .call(xAxisCall)
        .selectAll("text")
            .attr("y", "10")
            .attr("x", "-5")
            .attr("text-anchor", "end")
            .attr("transform", "rotate(-40)");

    var yAxisCall = d3.axisLeft(y)
        .ticks(6) //number of elements in which it will break the value
        .tickFormat(function(d){
            return d + "m";
        });
    g.append("g")
        .attr("class", "y-axis")
        .call(yAxisCall);



    // =========================
    // 3.4. input of values elements
    // =========================

    var rects = g.selectAll("rect")
        .data(data)
    
    rects.enter()
        .append("rect")
            .attr("y", function(d){ return y(d.revenue); })
            .attr("x", function(d){ return x(d.month); })
            .attr("width", x.bandwidth)
            .attr("height", function(d){ return height - y(d.revenue); })
            .attr("fill", function(d){ 
                console.log("com " + d.month + " a cor retornada: " + scaleOrdinal_color(d.nomth))
                return scaleOrdinal_color(d.month)
            });

    // addRules()

})