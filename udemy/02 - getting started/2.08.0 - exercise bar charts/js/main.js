/*
*    main.js
*    Mastering Data Visualization with D3.js
*    2.8 - Activity: Your first visualization!
*/

d3.json("data/buildings.json").then(function(data){
    console.log(data)

    data.forEach(function(d){
        d.height = +d.height
    });

    svg = d3.select("#chart-area").append("svg")
    .attr("width", 1000)
    .attr("height", 500)

    addRules(50)

    //<rect x="0" y="0" width="50" height="50" fill="green"></rect>
    var rects = svg.selectAll("rect").data(data);

    rects.enter()
        .append("rect")
            .attr("width", 40)
            .attr("height", function(d){
                return d.height
            })
            .attr("x", function(d,i){
                return i * 50 + 20
            })
            .attr("y", 0)
            .attr("fill", "gray");
    
}).catch(function(error){
    console.log(error);
});