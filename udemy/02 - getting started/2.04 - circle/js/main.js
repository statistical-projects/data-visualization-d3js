/*
*    main.js
*    Mastering Data Visualization with D3.js
*    2.4 - Adding SVGs with D3
*/



var svg = 
	d3.select("#chart-area")
	.append("svg")
	.attr("class", "item")
	.attr("width", 400)
	.attr("height", 400);

addRules(100)

var circle = 
	svg.append("circle")
	.attr("class", "item")
	.attr("cx", 200)
	.attr("cy", 200)
	.attr("r", 70)
	.attr("fill", "green");





//d3.selectAll("#chart-area .item").attr("style", "outline: 1px solid black")