function addRules(factor = 50){
	width = svg.attr("width")
	height = svg.attr("height")
	offset = factor/2

	svg.attr("style", "outline: 1px solid gray")

	//prepare the y axis
	for(i = 0; i <= height; i = i + factor){
	
		x1 = 0;
		x2 = width;
		y1 = i;
		y2 = i;
	
		svg.append("line")
		.attr("class", "item")
		.attr("x1", x1)
		.attr("x2", x2)
		.attr("y1", y1)
		.attr("y2", y2)
		.attr("stroke", "gray")

		svg.append("text")
		.attr("x", x1)
		.attr("y", y1)
		.text(y2)
		.attr("font-size", "10px")
		.attr("fill", "gray");
	}

	//prepare the x axis
	for(i = 0; i <= width; i = i + factor){
	
		x1 = i;
		x2 = i;
		y1 = 0;
		y2 = height;
	
		svg.append("line")
		.attr("class", "item")
		.attr("x1", x1)
		.attr("x2", x2)
		.attr("y1", y1)
		.attr("y2", y2)
		.attr("stroke", "gray")

		svg.append("text")
		.attr("x", x1)
		.attr("y", y1)
		.text(x2)
		.attr("font-size", "10px")
		.attr("fill", "gray")
		.attr("dy", ".75em")
	}

}
