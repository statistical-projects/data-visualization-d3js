function addRules(factor = 50, element = svg, color = "gray"){
	var width = element.attr("width")
	var height = element.attr("height")
	var offset = factor/2

	element.attr("style", "outline: 1px solid " + color)
	
	element.selectAll("g")
	.attr("style", "outline: 1px solid " + "orange")

	//prepare the y axis
	for(i = 0; i <= height; i = i + factor){
	
		x1 = 0;
		x2 = width;
		y1 = i;
		y2 = i;
	
		element.append("line")
		.attr("class", "item")
		.attr("x1", x1)
		.attr("x2", x2)
		.attr("y1", y1)
		.attr("y2", y2)
		.attr("stroke", color)

		element.append("text")
		.attr("x", x1)
		.attr("y", y1)
		.text(y2)
		.attr("font-size", "10px")
		.attr("fill", color);
	}

	//prepare the x axis
	for(i = 0; i <= width; i = i + factor){
	
		x1 = i;
		x2 = i;
		y1 = 0;
		y2 = height;
	
		element.append("line")
		.attr("class", "item")
		.attr("x1", x1)
		.attr("x2", x2)
		.attr("y1", y1)
		.attr("y2", y2)
		.attr("stroke", color)

		element.append("text")
		.attr("x", x1)
		.attr("y", y1)
		.text(x2)
		.attr("font-size", "10px")
		.attr("fill", color)
		.attr("dy", ".75em")
	}

}


function removeDuplicates(originalArray) {
	var newArray = [];
	var lookupObject  = {};

	for(var i in originalArray) {
	   lookupObject[originalArray[i]] = originalArray[i];
	}

	for(i in lookupObject) {
		newArray.push(lookupObject[i]);
	}
	 return newArray;
}