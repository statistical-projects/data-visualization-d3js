/*
*    main.js
*    Mastering Data Visualization with D3.js
*    Project 2 - Gapminder Clone
*/

d3.json("data/data.json").then(function(data){
	console.log(data);
})





// =========================
// 1. Params & definition of canvas
// =========================

var margin = { left:100, right:10, top:10, bottom:150 };

var width = 1000 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

// var flag = true;

var t = d3.transition().duration(100);

var svg = d3.select("#chart-area")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)

var g = svg.append("g")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");




// =========================
// 2. Definition of global elements
// =========================


    // Add legends to the graph
    // X Label
    g.append("text")
        .attr("class", "x axis-label")
        .attr("x", width / 2)
        .attr("y", height + 140)
        .attr("font-size", "20px")
        .attr("text-anchor", "middle")
        .text("GPD per capta ($)");

    // Y Label
    g.append("text")
        .attr("class", "y axis-label")
        .attr("x", - (height / 2))
        .attr("y", -60)
        .attr("font-size", "20px")
        .attr("text-anchor", "middle")
        .attr("transform", "rotate(-90)")
		.text("Life expectancy (years)");
		
	// Z Label
	g.append("text")
		.attr("class", "z axis-label")
		.attr("id", "year")
		.attr("x", 0)
		.attr("y", height + 140)
		.attr("font-size", "100px")
		.attr("text-anchor", "middle")
		.text("1800");


    // =========================
    // 2.2. scalings
    // =========================

    // X Scale
    var x = d3.scaleLog()
	.range([0, width])
	.base(10);

    // Y Scale
    var y = d3.scaleLinear()
	.range([height, 0]);
	
	// Y Scale
    var z = d3.scaleLinear()
	.range([25*Math.PI, 1500*Math.PI]);


    // =========================
    // 2.3. axis
    // =========================

    var xAxisGroup = g.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height +")");

    var yAxisGroup = g.append("g")
        .attr("class", "y axis");



// =========================
// 3. Plot
// =========================

d3.json("data/data.json").then(function(data){

    // =========================
    // 3.1. import & data prep
    // =========================

	console.log(data);
	
    //cast the height value into integer
    data.forEach(function(d){
		d.year = +d.year;
		d.countries.income = +d.countries.income;
		d.countries.life_exp = +d.countries.life_exp;
		d.countries.population = +d.countries.population;
	});
	
	console.log(data[0].year)
	console.log(data[0].countries)

    // =========================
    // 3.1. interval for updates
    // =========================

	i = 0;
	i_max = data.length;
    interval = d3.interval(function(){
        var newData = data[i];
		update(newData.countries)
		d3.select("#year").text(newData.year);

		i++
		if(i >= i_max){
			clearInterval(interval);
		}
    }, 10000);

    //addRules()
});






function update(data){
    // field used for value in y axis
	x_value = "income"
	y_value = "life_exp"
	z_value = "population"

    // =========================
    // 3.2. scalings
    // =========================

    //scale the category 
	x.domain([
		300, 
        150000
    ]);

    //scale the numeric values
    y.domain([
        0, 
		90
	]);
	
	//scale the numeric values
    z.domain([
        0, 
		1389618778
    ]);

	//ORDINAL SCALE - color
	continent_map = data.map(function(d){return d.continent})
	unique_continent_map = removeDuplicates(continent_map)

	var scaleOrdinal_color = d3.scaleOrdinal()
        .domain(unique_continent_map) // values from dataset
		.range(d3.schemeYlOrRd[unique_continent_map.length]); // color in the chromatic option

    // =========================
    // 3.3. axis
    // =========================
    
    var xAxisCall = d3.axisBottom(x)//receive the object which is the axis        
		.ticks(2) //number of elements in which it will breaking the value
		.tickFormat(function(d){
			return "$" + d;
		});
    ;
    xAxisGroup.attr("transform", "translate(0, " + height + ")") // position into the 0, bottom value
        .call(xAxisCall);

    var yAxisCall = d3.axisLeft(y)
    // var yAxisCall = d3.axisRight(y)
        .ticks(10) //number of elements in which it will breaking the value
        .tickFormat(function(d){
            return d;
        });
    yAxisGroup.call(yAxisCall);


    // =========================
    // 3.4. clean and input of values elements
    // =========================


   // scatter plot
   
   var circles = g.selectAll("circle")
		.data(data, function(d){
			return d.country;
		});
    
    // EXIT old elements not present in new data.
    circles.exit()
        .attr("fill", "red")
    .transition(t)
        .attr("y", y(0))
        .remove();

     // ENTER new elements present in new data...
     circles.enter()
     .append("circle")
        .attr("fill", function(d){ 
            return scaleOrdinal_color(d.country)
        })
		.attr("cx", function(d){ return x(d[x_value])})
		.attr("cy", function(d){ return y(d[y_value])})
		.attr("r", function(d){ return Math.sqrt(z(d[z_value]) / Math.PI) })
        // AND UPDATE all (old and new) elements present in new data.
        .merge(circles)
        .transition(t) //comment to take off the transition
            .attr("cx", function(d){ return x(d[x_value])})
            .attr("cy", function(d){ return y(d[y_value])})
            .attr("r", function(d){ return Math.sqrt(z(d[z_value]) / Math.PI) })
}
